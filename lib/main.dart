import 'package:flutter/material.dart';
//import 'globals.dart' as globals;
//import 'package:simple_application_v2/custom_icons_icons.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('A Wild Monster has Appeared!', style: TextStyle(
                      fontWeight: FontWeight.bold),
                  ),
                ),
                Text('What will you do?', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Place Holder from previous',
        softWrap: true,
      ),
    );
    Widget overlapImg = Container(
      child: Stack(
        children: [
          Image.asset(
            'images/battle1.png',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'images/DQ_Slime.png',
            width: 600,
            height: 200,
            fit: BoxFit.fitHeight,
          ),
        ],
      ),
    );


    return MaterialApp(
      title: 'JRPG BATTLE SIM',
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('BATTLE!'),
          backgroundColor: Colors.blueAccent,
        ),
        body: ListView(
          children: [
            overlapImg,
            titleSection,
            ButtonWidget(),
          ],
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}


class ButtonWidget extends StatefulWidget {

  ButtonWidget();

  @override
  _ButtonWidgetState createState() => _ButtonWidgetState();
}
class _ButtonWidgetState extends State<ButtonWidget> {
  bool _buttonPress = true;
  bool _buttonPress2 = true;
  int enemyHp = 100;

  @override
  Widget build(BuildContext context) {
    //debugPrint('OI');
    return
      Container(
        child: Column(
          children: [
            Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 40),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: (_buttonPress ? Icon(Icons.highlight_off) : Icon(Icons.cancel)),
                        color: Colors.red,
                        onPressed: _toggleButton,
                      ),
                    ),
                    SizedBox(
                      child: Container(
                        child: Text('Attack',
                          style: TextStyle(color: Colors.red,),),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 40),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: (_buttonPress2 ? Icon(Icons.brightness_5) : Icon(Icons.brightness_7)),
                        color: Colors.blue,
                        onPressed: _toggleButton2,
                      ),
                    ),
                    SizedBox(
                      child: Container(
                        child: Text('Magic',
                          style: TextStyle(color: Colors.blue,),),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('ENEMY HP: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25), ),
                Text('$enemyHp', style: TextStyle(fontSize: 25, color: Colors.red),),
              ],
            ),
        ],
        ),
    );
  }

  void _toggleButton() {
    setState(() {
      debugPrint('State Primary');

      if (_buttonPress) {
        debugPrint('Attack State');
        enemyHp += -10;
        _buttonPress = false;
      }
      else {
        _buttonPress = true;
        enemyHp += -10;
      }

      _check();
    });
  }

  void _toggleButton2() {
    setState(() {
      if (_buttonPress2) {
        debugPrint('Attack State');
        enemyHp += -20;
        _buttonPress2 = false;
      }
      else {
        _buttonPress2 = true;
        enemyHp += -20;
      }

      _check();
    });
  }


void _check()
    {
      setState(() {
      if(enemyHp <= 0) {
        debugPrint('HP REFRESH');
        enemyHp = 100;
      }
    });
  }
}
